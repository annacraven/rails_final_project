json.extract! event, :id, :title, :when, :creator, :created_at, :updated_at
json.url event_url(event, format: :json)
