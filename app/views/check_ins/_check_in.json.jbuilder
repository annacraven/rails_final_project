json.extract! check_in, :id, :name, :email, :event_id, :created_at, :updated_at
json.url check_in_url(check_in, format: :json)
