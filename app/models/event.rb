class Event < ApplicationRecord
	has_many :check_ins, dependent: :destroy
	accepts_nested_attributes_for :check_ins, allow_destroy: true
	validates :title, presence: true, length: { maximum: 255 } 
	validates :date, presence: true
	validates :creator, presence: true, length: { maximum: 50 }

	# ValidatesTimeliness gem
	validates_datetime :date, :on_or_after => :today
	

end
