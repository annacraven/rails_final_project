class CheckIn < ApplicationRecord
	belongs_to :event

	before_save { self.email = self.email.downcase } # should be ok not testing this
	validates :name, presence: true, length: { maximum: 50 }
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :email, presence: true, 
					  length: { maximum: 255 },
					  format: { with: VALID_EMAIL_REGEX },
					  uniqueness: { case_sensitive: false, scope: :event_id }
	validates :event_id, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }

end

