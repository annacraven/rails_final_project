class FixColumnName < ActiveRecord::Migration[5.1]
  def change
  	rename_column :events, :when, :date
  end
end
