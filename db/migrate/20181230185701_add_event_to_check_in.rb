class AddEventToCheckIn < ActiveRecord::Migration[5.1]
  def change
  	remove_column :check_ins, :event_id
    add_reference :check_ins, :event, foreign_key: true
  end
end
