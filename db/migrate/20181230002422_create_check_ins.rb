class CreateCheckIns < ActiveRecord::Migration[5.1]
  def change
    create_table :check_ins do |t|
      t.string :name
      t.string :email
      t.integer :event_id

      t.timestamps
    end
  end
end
