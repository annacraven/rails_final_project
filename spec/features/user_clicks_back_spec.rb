require "rails_helper"

RSpec.feature "User clicks back" do
	context "from show event" do
		scenario "they should see the homepage" do
			event = create(:event)
			visit event_path(event.id)

			should_go_back_to_homepage
		end
	end

	context "from edit event" do
		scenario "they should see the homepage" do
			event = create(:event)
			visit edit_event_path(event.id)

			should_go_back_to_homepage
	
		end
	end

	context "from attendees" do
		scenario "they should see the homepage" do
			event = create(:event)
			visit event_check_ins_path(event.id)

			should_go_back_to_homepage
		end
	end

	context "from edit attendees" do
		scenario "they should see the attendees" do
			event = create(:event)
			check_in = create(:check_in, event_id: event.id)
			visit edit_check_in_path(check_in.id)

			click_on "Back"

			expect(page).to have_content "Attendees for"
			expect(page).to have_content event.title
		end
	end

	private
		def should_go_back_to_homepage
			click_on "Back to All Events"
			expect(page).to have_content "Events"
		end
	

end
