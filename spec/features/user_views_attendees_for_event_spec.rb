require "rails_helper"

RSpec.feature "User views attendees for event" do
	scenario "they see the attendees for that event" do
		event = create(:event)
		check_in = create(:check_in, event_id: event.id)
		visit event_path(event.id)
		click_on "Attendees"

		expect(page).to have_content check_in.name
		expect(page).to have_content check_in.email
	end

	scenario "they do not see attendees for other events" do
		event = create(:event)
		event2 = create(:event, title: "title2")
		check_in = create(:check_in, event_id: event.id)
		check_in2 = create(:check_in, name: "name2", event_id: event2.id)

		visit event_path(event.id)
		click_on "Attendees"

		expect(page).not_to have_content check_in2.name
		expect(page).to have_content check_in.name
	end
end
