require "rails_helper"

RSpec.feature "User clicks on show for event" do
	scenario "they see the correct event" do
		event = create(:event)
		visit root_path

		within "tr##{event.id}" do
			click_on "Show"
		end

		expect(page).to have_text "Title: #{event.title}"
		expect(page).to have_text "Date: #{event.date.strftime("%B %d, %Y")}"
		expect(page).to have_text "#{event.date.strftime("%l:%M%p")}"
		expect(page).to have_text "Creator: #{event.creator}"

	end
end
