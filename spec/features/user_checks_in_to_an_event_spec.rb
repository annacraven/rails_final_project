require "rails_helper"

RSpec.feature "User checks in to an event" do 
	context "the form is valid" do
		scenario "they are shown their check_in" do
			event = create(:event)
			visit event_path(event.id)
			click_on "Check In"

			check_in_name = "name"
			check_in_email = "email@email.com"

			fill_in "check_in_name", with: check_in_name
			fill_in "check_in_email", with: check_in_email
			select event.title, from: "check_in[event_id]"
			click_on "Create Check in"

			# should I also check that it is the correct event? correct time?
			expect(page).to have_content "Check in was successfully created"
		end
	end

	context "the form is invalid" do
		scenario "they see a useful error message" do
			event = create(:event)
			visit event_path(event.id)
			click_on "Check In"

			check_in_name = "name"
			fill_in "check_in_name", with: check_in_name
			click_on "Create Check in"

			expect(page).to have_content "Email can't be blank"
		end
	end

	context "they try to check into the same event twice" do
		scenario "they fail" do
			event = create(:event)

			check_in(event)
			check_in(event)

			# should I make sure that nothing was added to database?
			expect(page).to have_content "Email has already been taken"
		end

		context "with their email as capitals" do
			scenario "they fail" do
				event = create(:event)
				check_in(event)

				visit new_event_check_in_path(event.id)

				check_in_name = "name"
				check_in_email = "EMAIL@email.com"

				fill_in "check_in_name", with: check_in_name
				fill_in "check_in_email", with: check_in_email
				select event.title, from: "check_in[event_id]"
				click_on "Create Check in"

				expect(page).to have_content "Email has already been taken"
			end
		end
	end

	context "they try to check into a different event with the same email" do
		scenario "they succeed" do
			event = create(:event)
			check_in(event)
			event2 = create(:event, title: "title2")
			check_in(event2)

			expect(page).to have_content "Check in was successfully created"
		end
	end

	private
		def check_in(event)
			visit new_event_check_in_path(event.id)

			check_in_name = "name"
			check_in_email = "email@email.com"

			fill_in "check_in_name", with: check_in_name
			fill_in "check_in_email", with: check_in_email
			select event.title, from: "check_in[event_id]"
			click_on "Create Check in"
		end
end
