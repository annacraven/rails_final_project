require "rails_helper"

RSpec.feature "User views events homepage" do
	scenario "they see existing events" do
		# FactoryBot.create
		event = create(:event)

		visit root_path
		expect(page).to have_content event.title
		expect(page).to have_content event.date.strftime("%B %d, %Y")
		expect(page).to have_content event.creator
	end
end