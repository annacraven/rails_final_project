require "rails_helper"

RSpec.feature "User updates check_in" do
	context "the form is valid" do
		scenario "the event is updated" do
			event = create(:event)
			check_in = create(:check_in, event_id: event.id)

			visit event_check_ins_path(event.id)
			within "tr##{check_in.id}" do
				click_on "Edit"
			end

			old_email = check_in.email
			new_email = "emailnew@email.com"
			fill_in "Email", with: new_email
			click_on "Update Check in"

			expect(page).to have_content "Check in was successfully updated."
			expect(page).to have_content new_email
			expect(page).not_to have_content old_email

		end
	end

	context "the form is invalid" do
		scenario "they see a useful error message" do
			event = create(:event)
			check_in = create(:check_in, event_id: event.id)

			visit check_in_path(check_in.id)
			click_on "Edit"

			fill_in "Name", with: ""
			click_on "Update Check in"

			expect(page).to have_content "Name can't be blank"
		end
	end
end
