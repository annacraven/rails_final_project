# spec/features/user_creates_an_event.rb

require "rails_helper"

RSpec.feature "User creates an event" do 
	context "the form is valid" do
		scenario "they see the page for the created event" do
			title = "Title"
			creator = "Creator"

			visit root_path
			click_on "New Event"
			fill_in "event_title", with: title
			# don't need to fill in date, because it's automatically set to now
			fill_in "event_creator", with: creator
			click_on "Create Event"

			expect(page).to have_content("Event was successfully created.")
		end
	end

	context "the form is invalid" do
		scenario "they see a useful error message" do
			creator = "Creator"

			visit root_path
			click_on "New Event"
			fill_in "event_creator", with: creator
			click_on "Create Event" 

			expect(page).to have_content "Title can't be blank"
		end
	end

	
end