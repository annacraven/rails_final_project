require "rails_helper"

RSpec.feature "User updates event" do
	context "the form is valid" do
		scenario "the event is updated" do
			event = create(:event)
			visit root_path
			within "tr##{event.id}" do
				click_on "Edit"
			end

			old_title = event.title
			new_title = "New Title"
			fill_in "Title", with: new_title
			year = event.date.year
			year = year + 1
			select year, from: "event_date_1i"
			click_on "Update Event"

			expect(page).to have_content "Event was successfully updated."
			expect(page).to have_content new_title
			expect(page).not_to have_content old_title
		end
	end

	context "the form is invalid" do
		scenario "they see a useful error message" do
			event = create(:event)
			visit event_path(event.id)

			click_on "Edit Event"
			fill_in "Title", with: ""
			click_on "Update Event"

			expect(page).to have_content "Title can't be blank"
		end
	end
end