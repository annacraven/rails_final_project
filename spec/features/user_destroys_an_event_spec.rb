require "rails_helper"

RSpec.feature "user_destroys an event", js: true do
	scenario "they no longer see that event on the homepage" do
		skip "need a driver that supports javascript"
		event = create(:event)
		visit root_path
		expect(page).to have_content event.title

		page.accept_confirm do
			within "tr##{event.id}" do
				click_on "Destroy"
			end
		end

		# # override the alert box to always return true
		# page.evaluate_script('window.confirm = function() { return true; }')

		# within "tr##{event.id}" do
		# 	click_on "Destroy"
		# end
		# # page.driver.accept_js_confirms

		# page.accept_alert "Are you sure?" do
		# 	click_button "Ok"
		# end

		expect(page).not_to have_content event.title

	end

	scenario "corresponding check_ins are also destroyed" do
		skip "need a driver that supports javascript"
	end
end