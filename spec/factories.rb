FactoryBot.define do 
	factory :event do
		title { "title" }
		date { DateTime.current.tomorrow }
		creator { "creator" }
	end

	factory :check_in do
		name { "name" }
		email { "email@email.com" }
		event
	end
end