# spec/models/event_spec.rb
require "rails_helper"

# event has many check_ins
RSpec.describe Event, type: :model do
	it { should have_many(:check_ins).dependent(:destroy) }
end

# event accepts validations for check_ins
RSpec.describe Event, type: :model do
	# allow destroy, true
	it { should accept_nested_attributes_for(:check_ins).allow_destroy(true) }
end

# test validations
RSpec.describe Event, "validations", type: :model do
	# validate title
	it { should validate_presence_of(:title) }
	it { should validate_length_of(:title).is_at_most(255) }

	# validate date
	it { should validate_presence_of(:date) }
	it "requires a date be on or after today" do
		event = Event.new(date: DateTime.current.prev_day)
		expect(event).not_to be_valid
	end

	# validate creator
	it { should validate_presence_of(:creator) }
	it { should validate_length_of(:creator).is_at_most(50) }
end
