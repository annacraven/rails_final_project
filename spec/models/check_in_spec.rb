# spec/models/check_in_spec.rb
# shoulda-matchers

require "rails_helper"

RSpec.describe CheckIn, type: :model do
	it { should belong_to(:event) }
end

RSpec.describe CheckIn, type: :model do
	# name validations
	it { should validate_presence_of(:name) }
	it { should validate_length_of(:name).is_at_most(50) }

	# email validations
	it { should validate_presence_of(:email) }
	it { should validate_length_of(:email).is_at_most(255) }
	# format with VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	it { should allow_values("user@example.com", "USER@foo.COM", "A_US-ER@foo.bar.org",
                           "first.last@foo.jp", "alice+bob@baz.cn").for(:email) }
	it { should_not allow_values("user@example,com", "user_at_foo.org", "user.name@example.
	                       foo@bar_baz.com", "foo@bar+baz.com").for(:email) }
	# subject { FactoryBot.create(:check_in) } # using FactoryBot
	it { should validate_uniqueness_of(:email).case_insensitive.scoped_to(:event_id) }
	# if you get this error:
		# PG::NotNullViolation: ERROR:  null value in column "content" violates not-null constraint
	    # DETAIL:  Failing row contains (1, null, null).
	    # : INSERT INTO "posts" DEFAULT VALUES RETURNING "id"
	    # Then go here: https://github.com/thoughtbot/shoulda-matchers/blob/master/lib/shoulda/matchers/active_record/validate_uniqueness_of_matcher.rb

	# creator validations
	it { should validate_presence_of(:event_id) }
	# this one I shouldn't really need
	it { should validate_numericality_of(:event_id).only_integer.is_greater_than_or_equal_to(0) }
end

