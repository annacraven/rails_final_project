# README

Event Registration App

* Project Description:
	This is a Rails app that implements an event registration system. Users create or cancel events, then check in to events. Their check in times are recorded, and a list of attendees is available for each event.

* Models:
	- The application uses two models, events and check_ins. 
		- Event:
			title:String, when:Datetime, creator:String
		- CheckIn:
			name:String, email:String, event_id:Integer

	- Each check_in belongs to an event, and each event has many check_ins. 
	- Deleting an event will delete the associated check_ins. 
	- Duplicate check_ins for the same event are not valid. 
	- Event.when must be on or after today.
	- All of the times are UTC.

* Routing:
	For check_ins, :index and :new are nested routes within an event.

* Ruby version: 2.5.1

* How to run the test suite:
	rails test
	rspec

* Additional Gems:
	- gem 'bootstrap-sass', '3.3.7'
	- gem 'validates_timeliness', '~> 4.0'
