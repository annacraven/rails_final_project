require 'test_helper'

class CheckInsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @event = events(:one)
    @event.save
    @check_in = check_ins(:one)
    @check_in.event_id = @event.id
  end


  test "should get index" do
    get event_check_ins_url(@event)
    assert_response :success
  end

  test "should get new" do
    get new_event_check_in_url(@event)
    assert_response :success
  end

  test "should create check_in" do
    assert_difference('CheckIn.count') do
      post check_ins_url, params: { check_in: { email: @check_in.email, event_id: @check_in.event_id, name: @check_in.name } }
    end

    assert_redirected_to check_in_url(CheckIn.last)
  end

  test "should show check_in" do
    @check_in.save
    get check_in_url(@check_in)
    assert_response :success
  end

  test "should get edit" do
    get edit_check_in_url(@check_in)
    assert_response :success
  end

  test "should update check_in" do
    patch check_in_url(@check_in), params: { check_in: { email: @check_in.email, event_id: @check_in.event_id, name: @check_in.name } }
    assert_redirected_to check_in_url(@check_in)
  end

  test "should destroy check_in" do
    assert_difference('CheckIn.count', -1) do
      delete check_in_url(@check_in)
    end

    assert_redirected_to check_ins_url
  end
  
end
