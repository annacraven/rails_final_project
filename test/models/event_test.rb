require 'test_helper'

class EventTest < ActiveSupport::TestCase

	def setup
		@event = events(:one)
	end

	test "should be valid" do
		assert @event.valid?
	end

	# test validate presence
	test "title should be present" do
		@event.title = "	"
		assert_not @event.valid?
	end

	test "date should be present" do
		@event.date = " "
		assert_not @event.valid?
	end

	test "creator should be present" do
		@event.creator = "	"
		assert_not @event.valid?
	end

	# test validate length
	test "title should not be too long" do
    	@event.title = "a" * 256
    	assert_not @event.valid?
  	end

    test "creator should not be too long" do
        @event.creator = "a" * 51
        assert_not @event.valid?
    end

    test "date should not be in the past" do
    	@event.date = DateTime.current.prev_day
    	assert_not @event.valid?
    end

    test "delete event should delete check_ins" do
    	@event.save
    	assert Event.exists?(@event.id)
    	# # use the CheckIn fixture called "one", found in test/fixtures
    	@check_in = check_ins(:one)
    	@check_in.event_id = @event.id
    	@check_in.save
    	assert CheckIn.exists?(@check_in.id)
    	assert_difference 'CheckIn.count', -1  do
    		@event.destroy
    	end
    end



end
