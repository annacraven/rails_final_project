require 'test_helper'

class CheckInTest < ActiveSupport::TestCase
	def setup
		@event = events(:one)
		@event.save
        @check_in = check_ins(:one)
        @check_in.event_id = @event.id
    end

  	test "should be valid" do
		assert @check_in.valid?
	end

	# test validate presence
	test "name should be present" do
		@check_in.name = "	"
		assert_not @check_in.valid?
	end

	test "email should be present" do
		@check_in.email = " "
		assert_not @check_in.valid?
	end

	test "event_id should be present" do
		@check_in.event_id = "	"
		assert_not @check_in.valid?
	end

	# test validate length
	test "name should not be too long" do
	    @check_in.name = "a" * 51
	    assert_not @check_in.valid?
  	end

	test "email should not be too long" do
	    @check_in.email = "a" * 244 + "@example.com"
	    assert_not @check_in.valid?
    end

    # test validate email address
    test "email validation should accept valid addresses" do
        valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                           first.last@foo.jp alice+bob@baz.cn]
        valid_addresses.each do |valid_address|
            @check_in.email = valid_address
      	    assert @check_in.valid?, "#{valid_address.inspect} should be valid"
        end
    end

	test "email validation should reject invalid addresses" do
		invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
	                       foo@bar_baz.com foo@bar+baz.com]
		invalid_addresses.each do |invalid_address|
	  		@check_in.email = invalid_address
	  		assert_not @check_in.valid?, "#{invalid_address.inspect} should be invalid"
		end
	end

	# test don't let someone check in twice to the same event
	test "don't allow double check in for same event" do
		duplicate_check_in = @check_in.dup
		duplicate_check_in.email = @check_in.email.upcase
		@check_in.save
		assert_not duplicate_check_in.valid?
	end

	test "allow same email to check into different events" do
		duplicate_check_in = @check_in.dup
		# add another event, test/fixtures/events.yml
		@event2 = events(:two)
		@event2.save
		duplicate_check_in.event_id = @event2.id
		@check_in.save
		assert duplicate_check_in.valid?
	end

	test "only allow integers greater than 0 for event_id" do
		@check_in.event_id = -1
		assert_not @check_in.valid?
	end

end
