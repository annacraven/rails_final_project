require 'test_helper'

class EventFlowTest < ActionDispatch::IntegrationTest

	def setup
		@event = events(:one)
	end

	test "invalid event creation" do
		get new_event_path
		assert_no_difference 'Event.count' do
			post events_path, params: { event: { title: "",
										date: DateTime.current,
										creator: "" } }
		end
		assert_template 'events/new'
	end

	test "can create event" do
		get new_event_path
		assert_difference 'Event.count', 1 do
			create_new_event
		end
		assert_response :redirect
		follow_redirect!
		assert_template 'events/show'
	end

	test "can destroy event" do
		create_new_event
		assert_difference 'Event.count', -1 do
			delete event_path(@event)
		end
		assert_redirected_to events_path
	end

	test "can check into an event" do
		create_new_event
		assert_difference 'CheckIn.count', 1 do
			create_new_check_in
		end
		assert_response :redirect
		follow_redirect!
		assert_template 'check_ins/show'

	end

	test "can't check in more than once to the same event" do
		create_new_event
		create_new_check_in
		assert_no_difference 'CheckIn.count' do
			create_new_check_in
		end
	end

	test "view a list of attendees for event" do
		create_new_check_in
		get event_check_ins_url(@event)
    	assert_response :success
    	# number of rows should be same as number of check ins, plus one extra row for table headers
    	assert_select 'tr', CheckIn.where(event_id: @event.id).count + 1
	end

	private
		def create_new_event
			post events_path,
				params: { event: { title: @event.title, date: @event.date, creator: @event.creator} }
		end

		def create_new_check_in
			@check_in = check_ins(:one)
			@check_in.event_id = @event.id
			post check_ins_path,
				params: {check_in: { name: @check_in.name, 
								 	 email: @check_in.email, 
								 	 event_id: @check_in.event_id } }
		end


end
