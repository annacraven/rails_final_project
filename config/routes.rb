Rails.application.routes.draw do
  resources :events do
  	resources :check_ins, only: [:index, :new]
  end
  resources :check_ins, only: [:create, :show, :edit, :update, :destroy]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'events#index'

end
